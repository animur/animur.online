<?php
App::import('Vendor', 'JWT', array('file' => 'firebase/php-jwt/src/JWT.php'));
App::import('Vendor', 'SignatureInvalidException', array('file' => 'firebase/php-jwt/src/SignatureInvalidException.php'));
App::import('Vendor', 'ExpiredException', array('file' => 'firebase/php-jwt/src/ExpiredException.php'));



class Token
{
    private static $secret = 'ipkgh0qu()Y&+öm#V*5y0mPQmXI*P%';

    /**
     * Decode token with secret.
     *
     * @param string $token
     * @param string $model |null
     * @return array|bool
     */
    public static function decode(string $token, string $model = null)
    {
        try {
            $decoded = (array)\Firebase\JWT\JWT::decode($token, self::$secret, array('HS256'));

            // Confirm email if login data is set.
            if (is_string($model) &&
                isset($decoded['mail']) &&
                (isset($decoded['cid']) || isset($decoded['contact_id']))
            ) {
                if ($model == 'Candidate' && !empty($decoded['cid'])) {
                    $Model = ClassRegistry::init('User');

                    $conditions = [
                        'User.username' => $decoded['mail'],
                        'User.confirmed' => 0,
                        'User.candidate_id' => $decoded['cid']
                    ];

                    $who = [
                        'candidate_id' => $decoded['cid']
                    ];
                } elseif ($model == 'Contact' && !empty($decoded['contact_id'])) {
                    $Model = ClassRegistry::init('Contact');

                    $conditions = [
                        'Contact.email' => $decoded['mail'],
                        'Contact.confirmed' => 0,
                        'Contact.id' => $decoded['contact_id']
                    ];

                    $companyId = $Model->fieldById($decoded['contact_id'], 'company_id');

                    if (empty($companyId)) {
                        throw new NotFoundException('No company_id found for this Contact.');
                    }

                    $who = [
                        'contact_id' => $decoded['contact_id'],
                        'company_id' => $companyId
                    ];
                } else {
                    throw new InvalidArgumentException(
                        '$model must be "Contact" or "Candidate" and their field must be existent.'
                    );
                }

                $Model->updateAll(
                    ['confirmed' => 1],
                    $conditions
                );

                if ($Model->getAffectedRows() > 0) {
                    $ActivityLog = ClassRegistry::init('ActivityLog');
                    $ActivityLog->add(
                        'account_confirmed',
                        array_merge(
                            $who,
                            ['data' => $decoded['mail']]
                        )
                    );
                }

            }
        } catch (Exception $e) {
            error_log($e);
            return false;
        }

        return $decoded;
    }

    /**
     * Generates a token to login a contact or candidate.
     *
     * @param int $id
     * @param string $model Must be "Contact" or "Candidate"
     * @param bool $incognito if set to 1, activity and login count etc. will not be logged.
     * @return string
     * @throws InvalidArgumentException
     */
    public static function generateLoginToken(int $id, string $model, bool $incognito = false): string
    {
        if ($model == 'Contact') {
            $pk = 'contact_id';
        } elseif ($model == 'Candidate') {
            $pk = 'cid';
        } else {
            throw new InvalidArgumentException('$model must be "Contact" or "Candidate".');
        }

        $tokenData = array(
            $pk => $id,
            'login' => 1,
            'iat' => time(),
            'incognito' => $incognito
        );
        return self::encode($tokenData);
    }

    /**
     * Creates a new token with specific data.
     * Expires in two weeks if $valid is empty.
     *
     * @param $data
     * @param int $valid
     * @return string
     * @author Tilman Stremlau
     * @version 05.10.2015
     */
    static function encode($data, $valid = null)
    {
        if ($valid == null) {
            $valid = (60 * 60 * 24 * 14); //TODO fetch from settings table
        }

        $data = array_merge($data, array(
            'exp' => time() + $valid
        ));
        return \Firebase\JWT\JWT::encode($data, self::$secret);

    }
}