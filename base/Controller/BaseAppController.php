<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class BaseAppController extends Controller
{
    public $components = [
        'Flash',
        'Cookie',
        'Paginator',
        'Auth' => [
            'loginRedirect' => [
                'controller' => 'posts',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'pages',
                'action' => 'display',
                'home'
            ],
            'authenticate' => [
                'Form' => [
                    'passwordHasher' => 'Blowfish'
                ]
            ]
        ]
    ];

    public function beforeFilter()
    {
        $this->Auth->allow();
        $this->Auth->deny('add', 'edit');
        $this->loadModel('Credit');
        $cakeDescription = $this->Credit->field('data', ['name' => 'title']);
        $this->set("role", $this->Auth->user('role'));//it will set a variable role for your view
        $this->set(compact('cakeDescription'));
        $this->set('authUser', $this->Auth->user());
    }
}
