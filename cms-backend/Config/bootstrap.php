<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

$sharedAppFolderName = 'base';
$dirname = dirname(dirname(dirname(__FILE__)));
$basename = basename(dirname(dirname(__FILE__)));

App::build([
    'Lib' => [
        $dirname . DS . $basename . DS . 'Lib' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'Lib' . DS],
    'Error' => [
        $dirname . DS . $basename . DS . 'Lib' . DS . 'Error' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'Lib' . DS . 'Error' . DS],
    'Log' . DS . 'Engine' => [
        $dirname . DS . $sharedAppFolderName . DS . 'Lib' . DS . 'Log' . DS . 'Engine' . DS],
    'Controller' => [
        $dirname . DS . $basename . DS . 'Controller' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'Controller' . DS],
    'Controller' . DS . 'Component' => [
        $dirname . DS . $basename . DS . 'Controller' . DS . 'Component' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'Controller' . DS . 'Component' . DS],
    'Model' => [
        $dirname . DS . $basename . DS . 'Model' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'Model' . DS],
    'Model' . DS . 'Behavior' => [
        $dirname . DS . $basename . DS . 'Model' . DS . 'Behavior' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'Model' . DS . 'Behavior' . DS],
    'Vendor' => [
        $dirname . DS . $basename . DS . 'Vendor' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'Vendor' . DS],
    'View' => [
        $dirname . DS . $basename . DS . 'View' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'View' . DS],
    'View' . DS . 'Helper' => [
        $dirname . DS . $basename . DS . 'View' . DS . 'Helper' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'View' . DS . 'Helper' . DS],
    'View' . DS . 'Pdf' => [
        $dirname . DS . $basename . DS . 'View' . DS . 'Pdf' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'View' . DS . 'Pdf' . DS],
    'Plugin' => [
        $dirname . DS . $basename . DS . 'Plugin' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'Plugin' . DS],
    'Test' . DS . 'Case' . DS . 'Selenium' => [
        $dirname . DS . $basename . DS . 'Test' . DS . 'Case' . DS . 'Selenium' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'Test' . DS . 'Case' . DS . 'Selenium' . DS],
    'Test' . DS . 'Fixture' => [
        $dirname . DS . $basename . DS . 'Test' . DS . 'Fixture' . DS,
        $dirname . DS . $sharedAppFolderName . DS . 'Test' . DS . 'Fixture' . DS],
], App::RESET);


/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array(DS . 'path/to/models' . DS, DS . 'next/path/to/models' . DS),
 *     'Model/Behavior'            => array(DS . 'path/to/behaviors' . DS, DS . 'next/path/to/behaviors' . DS),
 *     'Model/Datasource'          => array(DS . 'path/to/datasources' . DS, DS . 'next/path/to/datasources' . DS),
 *     'Model/Datasource/Database' => array(DS . 'path/to/databases' . DS, DS . 'next/path/to/database' . DS),
 *     'Model/Datasource/Session'  => array(DS . 'path/to/sessions' . DS, DS . 'next/path/to/sessions' . DS),
 *     'Controller'                => array(DS . 'path/to/controllers' . DS, DS . 'next/path/to/controllers' . DS),
 *     'Controller/Component'      => array(DS . 'path/to/components' . DS, DS . 'next/path/to/components' . DS),
 *     'Controller/Component/Auth' => array(DS . 'path/to/auths' . DS, DS . 'next/path/to/auths' . DS),
 *     'Controller/Component/Acl'  => array(DS . 'path/to/acls' . DS, DS . 'next/path/to/acls' . DS),
 *     'View'                      => array(DS . 'path/to/views' . DS, DS . 'next/path/to/views' . DS),
 *     'View/Helper'               => array(DS . 'path/to/helpers' . DS, DS . 'next/path/to/helpers' . DS),
 *     'Console'                   => array(DS . 'path/to/consoles' . DS, DS . 'next/path/to/consoles' . DS),
 *     'Console/Command'           => array(DS . 'path/to/commands' . DS, DS . 'next/path/to/commands' . DS),
 *     'Console/Command/Task'      => array(DS . 'path/to/tasks' . DS, DS . 'next/path/to/tasks' . DS),
 *     'Lib'                       => array(DS . 'path/to/libs' . DS, DS . 'next/path/to/libs' . DS),
 *     'Locale'                    => array(DS . 'path/to/locales' . DS, DS . 'next/path/to/locales' . DS),
 *     'Vendor'                    => array(DS . 'path/to/vendors' . DS, DS . 'next/path/to/vendors' . DS),
 *     'Plugin'                    => array(DS . 'path/to/plugins' . DS, DS . 'next/path/to/plugins' . DS),
 * ));
 */

/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); // Loads a single plugin named DebugKit
 */

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter . By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *        'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *        'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *        array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *        array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
    'AssetDispatcher',
    'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
    'engine' => 'File',
    'types' => array('notice', 'info', 'debug'),
    'file' => 'debug',
));
CakeLog::config('error', array(
    'engine' => 'File',
    'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
    'file' => 'error',
));
