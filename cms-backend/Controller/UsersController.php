<?php
App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if ($this->request->is(['post', 'put'])) {

            if (!empty($id)) {
                $this->User->id = $id;
            } else {
                $this->User->create();
            }
            $this->request->data["User"]["password"];

            if ($this->User->save($this->request->data)) {
                $this->Flash->flashMessageSaved('');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->flashMessageNotSaved('');
            }
        } else {
            if (empty($id)) {
                $this->set(compact('options', 'email', 'username', 'password', 'photo'));
            } else {
                $options = ['conditions' => ['User.' . $this->User->primaryKey => $id]];
                $this->request->data = $this->User->find('first', $options);
            }
        }
        $this->loadModel('Role');
        $default = $this->request->data["User"]["role"] ?? 5;

        $queries = $this->Role->find('all', [
            'fields' => ['russian'],
            'order' => 'id ASC'
        ]);
        $i = 0;
        foreach ($queries as $query) {
            $role['name'] = $query['Role']['russian'];
            $role['value'] = ++$i;
            $roles[] = $role;
        }
        unset($i);
        $this->set(compact('roles'));
        $this->set('default', $default);

        $this->render('edit');
    }

    /**
     * delete method
     *
     * @param string $id
     * @return CakeResponse|null
     */
    public function delete($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
}
