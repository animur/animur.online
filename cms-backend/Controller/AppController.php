<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('BaseAppController', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends BaseAppController
{
    public function beforeFilter()
    {
        if ($this->Auth->user('role') != 1) {
            $loginToken = $this->params['url']['token'];;
            App::uses('Token', 'Lib');
            $loginData = Token::decode($loginToken);
            if ($loginData["role"] != 1) {
                $this->render('\Errors\forbidden');
            } else {
                $this->Auth->login($loginData);
            }
            $this->redirect('');
        }
    }
}

