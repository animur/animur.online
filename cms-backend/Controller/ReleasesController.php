<?php
App::uses('AppController', 'Controller');

/**
 * Releases Controller
 *
 * @property Release $Release
 * @property PaginatorComponent $Paginator
 */
class ReleasesController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->Release->recursive = 0;
        $this->set('releases', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        if (!$this->Release->exists($id)) {
            throw new NotFoundException(__('Invalid release'));
        }
        $options = array('conditions' => array('Release.' . $this->Release->primaryKey => $id));
        $this->set('release', $this->Release->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->Release->create();
            if ($this->Release->save($this->request->data)) {
                $this->Flash->success(__('The release has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The release could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if (!$this->Release->exists($id)) {
            throw new NotFoundException(__('Invalid release'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Release->save($this->request->data)) {
                $this->Flash->success(__('The release has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The release could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Release.' . $this->Release->primaryKey => $id));
            $this->request->data = $this->Release->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null)
    {
        $this->Release->id = $id;
        if (!$this->Release->exists()) {
            throw new NotFoundException(__('Invalid release'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Release->delete()) {
            $this->Flash->success(__('The release has been deleted.'));
        } else {
            $this->Flash->error(__('The release could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
}
