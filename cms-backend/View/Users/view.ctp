<div class="users view">
    <dl>
        <dt><?= __('Id'); ?></dt>
        <dd>
            <?= h($user['User']['id']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Права'); ?></dt>
        <dd>
            <td><?= h($user['User']['role'] ? 'Пользователь' : 'Администратор'); ?>&nbsp;</td>
            &nbsp;
        </dd>
        <dt><?= __('Почта'); ?></dt>
        <dd>
            <?= h($user['User']['email']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Ник'); ?></dt>
        <dd>
            <?= h($user['User']['username']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Пароль'); ?></dt>
        <dd>
            <?= h($user['User']['password']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Аватарка'); ?></dt>
        <dd>
            <?= h($user['User']['photo']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Регистрация'); ?></dt>
        <dd>
            <?= h($user['User']['created']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Изменялся'); ?></dt>
        <dd>
            <?= h($user['User']['modified']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?= __('Действия'); ?></h3>
    <ul>
        <li><?= $this->Html->link(__('Изменить'), array('action' => 'edit', $user['User']['id'])); ?> </li>
        <li><?= $this->Form->postLink(__('Удалить'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?> </li>
        <li><?= $this->Html->link(__('К списку пользователей'), array('action' => 'index')); ?> </li>
        <li><?= $this->Html->link(__('Добавить нового'), array('action' => 'add')); ?> </li>
    </ul>
</div>
