<div class="users index">
    <h2><?= __('Users'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('role') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th><?= $this->Paginator->sort('username') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th class="actions"><?= __('Действия') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?= h($user['User']['id']) ?></td>
                <td><?= h($user['User']['role']) ?></td>
                <td><?= h($user['User']['email']) ?></td>
                <td><?= h($user['User']['username']) ?></td>
                <td><?= h($user['User']['created']) ?></td>
                <td><?= h($user['User']['modified']) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Детали'), ['action' => 'view', $user['User']['id']]) ?>
                    <?= $this->Html->link(__('Изменить'), ['action' => 'edit', $user['User']['id']]) ?>
                    <?= $this->Form->postLink(__('Удалить'), ['action' => 'delete', $user['User']['id']], ['confirm' => __('Вы уверены что хотите удалить пользователя %s?', $user['User']['username'])]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?= $this->Paginator->counter([
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ]);?>    </p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('Прошлая'), [], null, ['class' => 'prev disabled']);
        echo $this->Paginator->numbers(['separator' => '']);
        echo $this->Paginator->next(__('Следующая') . ' >', [], null, ['class' => 'next disabled']);
        ?>
    </div>
</div>
<div class="actions">
    <h3><?= __('Действия'); ?></h3>
    <ul>
        <li><?= $this->Html->link(__('Добавить пользователя'), ['action' => 'edit']) ?></li>
    </ul>
</div>
