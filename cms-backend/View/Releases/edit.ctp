<div class="releases form">
    <?php echo $this->Form->create('Release'); ?>
    <fieldset>
        <legend><?php echo __('Edit Release'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('name');
        echo $this->Form->input('cover');
        echo $this->Form->input('header');
        echo $this->Form->input('description');
        echo $this->Form->input('owner');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Release.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Release.id')))); ?></li>
        <li><?php echo $this->Html->link(__('List Releases'), array('action' => 'index')); ?></li>
    </ul>
</div>
