<div class="releases form">
    <?php echo $this->Form->create('Release'); ?>
    <fieldset>
        <legend><?php echo __('Add Release'); ?></legend>
        <?php
        echo $this->Form->input('name');
        echo $this->Form->input('cover');
        echo $this->Form->input('header');
        echo $this->Form->input('description');
        echo $this->Form->input('owner');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Html->link(__('List Releases'), array('action' => 'index')); ?></li>
    </ul>
</div>
