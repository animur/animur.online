<?php
/**
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */

//if (!Configure::read('debug')) {
//    throw new NotFoundException();
//}

App::uses('Debugger', 'Utility');
?>
<h2><?= __d('cake_dev', 'Admin Backend.'); ?></h2>
<h3><?= __d('cake_dev', 'Cakephp version: %s.', Configure::version()); ?></h3>

<?php
if (Configure::read('debug') > 0) {
    Debugger::checkSecurityKeys();
}
?>
<p><?= $this->Html->link(__('Пользователи'), ['action' => 'index', 'controller' => 'users']); ?></p>
<p><?= $this->Html->link(__('Релизы'), ['action' => 'index', 'controller' => 'releases']); ?></p>