<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 */
class User extends AppModel
{

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'username' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Пожалуйста введите желаемый логин.',
                'allowEmpty' => false,
                'required' => true,
            ),
//            'noScriptTags' => array(
//                'rule' => array('validateAgainstScriptTag'),
//                'message' => 'Код запрещен;)',
//                'allowEmpty' => true
//            ),
        ),
        'email' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Пожалуйста введите ваш почтовый адрес',
                'allowEmpty' => false,
                'required' => true,
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Данный почтовый адрес уде занят.',

            ),
            'emailFormat' => array(
                'rule' => array('email', false),
                'message' => 'Пожалуйста введите почтовый адрес в верном формате',

            ),
        ),
        'password' => array(
            'between' => array(
                'rule' => array('minLength', 5),
                'message' => 'Пароль должен состоят как минимум из 5 символов',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        )
    );

    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
}
