<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Lib');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function beforeFilter()
    {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('add');
        $this->Auth->deny('view', 'index');
    }

    /**
     * @return CakeResponse|null
     */
    public function login()
    {
        if ($this->request->is('POST')) {
            if ($this->Auth->login()) {
                return $this->redirect('/');
            }
            $this->Flash->error(__('Неверный логин или пароль'));
            return $this->redirect(['controller' => 'pages', 'action' => 'display']);
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        if (!$this->Auth->user('id')) {
            throw new NotFoundException(__('Пользователь не найден'));
        }
        $options = [
            'conditions' => [
                'id' => $this->Auth->user('id')
            ]
        ];
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * register method
     */
    public function register()
    {
        if (AuthComponent::user()) {
            $this->Flash->set('Вы уже зарегистрированы');
            $this->redirect($this->referer());
        }
        if ($this->request->is('post')) {
            $this->User->create();
            $this->request->data["User"]["role"] = 5; // Only user can be registered from user-backend.
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Регистрация прошла успешно.'));
                $email = $this->request->data['email'];
                $this->User->findByEmail($email, 'id');
                $this->Auth->login();
                $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Регистрация не удалась. Пожалуйста попробуйте снова.'));
            }
        } else {
            return $this->set('data', $this->render('register'));
        }
    }

    /**
     * edit method
     *
     * @param string $id
     * @return CakeResponse|null
     */
    public function edit($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Такого пользователя не существует'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('Не удалось изменить пользователя.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

    public function adminRedirect()
    {
        if ($this->Auth->user('role') == 1) {
            App::uses('Token', 'Lib');
            $loginData = Token::encode($this->Auth->user());

            $this->redirect("http://admin.animur.local?token=$loginData");
        }
    }
}
