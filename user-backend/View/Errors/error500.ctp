<?php
/**
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 */
?>
    <p class="error" style="text-align: center">
        <?php echo __d('cake', 'Упс. Что-то пошло не так! Возможно этой страницы нет, а возможно вам туда нельзя=)'); ?>
    </p>
<?php
if (Configure::read('debug') > 0):
    echo $this->element('exception_stack_trace');
endif;
