<?php if (!$authUser) { ?>
    <div class="login-box" id="login-box" title="Авторизация">
        <div class="login-social clearfix">
            <?= $this->Html->link($this->Html->image(
                '../img/social/vkontakte.png'), '{vk_url}', ['escapeTitle' => false, 'target' => '_blank']
            ); ?>
            <?= $this->Html->link($this->Html->image(
                '../img/social/google.jpg'), '{vk_url}', ['escapeTitle' => false, 'target' => '_blank']
            ); ?>
            <?= $this->Html->link($this->Html->image(
                '../img/social/facebook.jpg'), '{vk_url}', ['escapeTitle' => false, 'target' => '_blank']
            ); ?>
            <?= $this->Html->link($this->Html->image(
                '../img/social/odnoklassniki.jpg'), '{vk_url}', ['escapeTitle' => false, 'target' => '_blank']
            ); ?>
            <?= $this->Html->link($this->Html->image(
                '../img/social/yandex.png'), '{vk_url}', ['escapeTitle' => false, 'target' => '_blank']
            ); ?>
            <?= $this->Html->link($this->Html->image(
                '../img/social/mailru.gif'), '{vk_url}', ['escapeTitle' => false, 'target' => '_blank']
            ); ?>

        </div>
        <div class="login-form">
            <?= $this->Flash->render('auth'); ?>
            <?= $this->Form->create('User', ['url' => array('controller' => 'users', 'action' => 'login')]); ?>
            <?= $this->Form->input('username', ['label' => false, 'placeholder' => 'Ваш логин']); ?>
            <?= $this->Form->input('password', ['label' => false, 'placeholder' => 'Ваш пароль']); ?>
            <div class="login-button">
                <?= $this->Form->button('Войти на сайт', [
                    'label' => __('Войти на сайт'),
                    'type' => 'submit',
                    'title' => 'Вход',
                ]); ?>
            </div>
            <?= $this->Form->end(); ?>
            <br>
            <div class="login-checkbox">
                <input type="checkbox" name="login_not_save" id="login_not_save" value="1"/>
                <label for="login_not_save">Чужой компьютер</label>
            </div>
            <div class="login-links clearfix">
                <a href="#">Забыли пароль?</a>
            </div>
            </form>
        </div>
    </div>
<?php } else { ?>
    <div class="login-box" id="login-box" title="<?= $authUser["username"]?>">
        <div class="login-avatar">
            <div class="avatar-box" id="avatar-box">
                <img src="<?= $authUser["photo"]?>" title="<?= $authUser["username"]?>" alt="<?= $authUser["username"]?>"/>
            </div>
            <?= $this->Html->link('Админпанель', ['controller' => 'users', 'action' => 'adminRedirect']); ?>
        </div>
        <ul class="login-menu">
            <?= $this->Html->link('Добавить пост', ''); ?>
            <?= $this->Html->link('Мой профиль', ''); ?>
            <?= $this->Html->link('Сообщения: ({new-pm})', ''); ?>
            <?= $this->Html->link('Мои закладки ', ''); ?>
            <?= $this->Html->link('Статистика', ''); ?>
            <?= $this->Html->link('Непрочитанное', ''); ?>
            <?= $this->Html->link('Последние комментарии', ''); ?>
            <?= $this->Html->link('Выйти', ['controller' => 'users', 'action' => 'logout']); ?>
        </ul>
    </div>
<?php } ?>