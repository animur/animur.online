﻿<div class="form-wrap">
    <h1>Регистрация</h1>
    <div class="full-text">
        <b>Здравствуйте, уважаемый посетитель нашего сайта!</b><br/>
        Регистрация на нашем сайте позволит Вам быть его полноценным участником.
        Вы сможете добавлять новости на сайт, оставлять свои комментарии, просматривать скрытый текст и многое другое.
        <br/>В случае возникновения проблем с регистрацией, обратитесь к <a
                href="/index.php?do=feedback">администратору</a> сайта.
        <b>Уважаемый посетитель,</b><br/>
        Ваш аккаунт был зарегистрирован на нашем сайте,
        однако информация о Вас является неполной, поэтому заполните дополнительные поля в Вашем профиле.
    </div>
        <?= $this->Form->create('User'); ?>
        <fieldset>
            <?= $this->Form->input('username', ['label' => false, 'div' => 'form-item clearfix imp', 'placeholder' => 'Логин']) ?>
            <?= $this->Form->input('password', ['label' => false, 'div' => 'form-item clearfix imp', 'placeholder' => 'Пароль']) ?>
            <?= $this->Form->input('email', ['label' => false, 'div' => 'form-item clearfix imp', 'placeholder' => 'Электронная почта']) ?>
            <?= $this->Form->button(__('Отправить'), ['type' => 'submit', 'name' => 'submit', 'div' => 'form-submit', 'style' => 'margin-left: 200px']) ?>
        </fieldset>
        <?= $this->Form->end() ?>
</div>