<div class="users view">
    <h2><?= __('User'); ?></h2>
    <dl>
        <dt><?= __('Id'); ?></dt>
        <dd>
            <?= h($user['User']['id']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Role'); ?></dt>
        <dd>
            <?= h($user['User']['role']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Email'); ?></dt>
        <dd>
            <?= h($user['User']['email']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Username'); ?></dt>
        <dd>
            <?= h($user['User']['username']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Password'); ?></dt>
        <dd>
            <?= h($user['User']['password']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Photo'); ?></dt>
        <dd>
            <?= h($user['User']['photo']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Created'); ?></dt>
        <dd>
            <?= h($user['User']['created']); ?>
            &nbsp;
        </dd>
        <dt><?= __('Modified'); ?></dt>
        <dd>
            <?= h($user['User']['modified']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?= __('Действия'); ?></h3>
    <ul>
        <li><?= $this->Html->link(__('Редактировать'), array('action' => 'edit', $user['User']['id'])); ?> </li>
    </ul>
</div>
