<div class="users form">
    <?= $this->Form->create('User'); ?>
    <fieldset>
        <legend><?= __('Add User'); ?></legend>
        <?php
        echo $this->Form->input('email');
        echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->input('photo');
        ?>
    </fieldset>
    <?= $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?= __('Actions'); ?></h3>
    <ul>

        <li><?= $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
    </ul>
</div>
