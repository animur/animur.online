<div class="users form">
    <?= $this->Form->create('User'); ?>
    <fieldset>
        <legend><?= __('Edit User'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('role');
        echo $this->Form->input('email');
        echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->input('photo');
        ?>
    </fieldset>
    <?= $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?= __('Actions'); ?></h3>
    <ul>

        <li><?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('User.id')))); ?></li>
        <li><?= $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
    </ul>
</div>
