<?php $cakeDescription = __d('cake_dev', 'Анимур') ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <?= $this->Html->charset(); ?>
    <title>
        <?= $cakeDescription ?>
    </title>
    <?php
    echo $this->Html->meta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0']);

    echo $this->Html->script(['jquery', 'lightbox/jquery.lightbox.min', 'my_ajax', 'filter-xf', 'bbcodes', 'filter-xf', 'jqueryui', 'typograf.min', 'dle_js', 'libs', 'lis-head', 'waypoints']);

    echo $this->Html->css(['jquery.lightbox', 'frame', 'preview', 'engine', 'cake.generic', 'filter-xf', 'offline', 'styles', 'lis-head', 'https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic&subset=latin,cyrillic']);

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>

</head>
<body class="js">
<div id="container">
    <?= $this->Session->flash(); ?>

    <?= $this->fetch('content'); ?>
</div>
</div>
<?= $this->element('sql_dump'); ?>
</body>
</html>
