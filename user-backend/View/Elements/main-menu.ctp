<ul class="main-menu clearfix icon-left" id="main-menu">
    <li><a href="#"><span class=""></span>Новости</a></li>
    <li><a href="#"><span class=""></span>Аниме по жанрам</a> <!-- пункт с подменю -->
        <ul class="hidden-menu clearfix">
            <li><a href="/xfsearch/janre/%D1%B8%ED%E5%ED/">Сёнэн</a></li>
            <li><a href="/xfsearch/janre/%F0%EE%EC%E0%ED%F2%E8%EA%E0/">Романтика</a></li>
            <li><a href="/xfsearch/janre/%EA%EE%EC%E5%E4%E8%FF/">Комедия</a></li>
            <li><a href="/xfsearch/janre/%D4%E0%ED%F2%E0%F1%F2%E8%EA%E0/">Фантастика</a></li>
            <li><a href="/xfsearch/janre/%F4%FD%ED%F2%E5%E7%E8/">Фэнтези</a></li>
            <li><a href="/xfsearch/janre/%EF%EE%E2%F1%E5%E4%ED%E5%E2%ED%EE%F1%F2%FC/">Повседневность</a></li>
            <li><a href="/xfsearch/janre/%F8%EA%EE%EB%E0/">Школа</a></li>
            <li><a href="#">Война</a></li>
            <li><a href="/xfsearch/janre/%F1%B8%E4%E7%B8/">Сёдзё</a></li>
            <li><a href="#">Детектив</a></li>
            <li><a href="/xfsearch/janre/%EF%F0%E8%EA%EB%FE%F7%E5%ED%E8%FF/">Приключения</a></li>
            <li><a href="/xfsearch/janre/%E4%F0%E0%EC%E0/">Драма</a></li>
            <li><a href="#">Музыкальный</a></li>
            <li><a href="#">Спорт</a></li>
            <li><a href="#">Махо-сёдзё</a></li>
            <li><a href="#">Боевые искусства</a></li>
            <li><a href="/">Сёдзё-ай</a></li>
            <li><a href="#">Сёнэн-ай</a></li>
            <li><a href="/xfsearch/janre/%EC%E8%F1%F2%E8%EA%E0/">Мистика</a></li>
            <li><a href="#">История</a></li>
        </ul>
    </li> <!-- конец пункт с подменю -->
    <li><a href="#"><span class=""></span>Аниме Фильмы</a></li>
    <li><a href="/anime/ova/"><span class=""></span>Аниме OVA</a></li>
    <li><a href="#"><span class=""></span> Команда</a></li>
    <li><a href="/"><span class=""></span>Расписание</a></li>  <!-- простой пункт -->
    <li><a href="#"><span class=""></span>Помощь ресурсу</a></li>
    <li class="sort-li"><a href="#"><span class="fa fa-sort-alpha-asc"></span></a>
        <div class="hidden-menu alpha-sort clearfix">
            <div class="flex-row">
                <a href="#">А</a>
                <a href="#">Б</a>
                <a href="#">В</a>
                <a href="#">Г</a>
                <a href="#">Д</a>
                <a href="#">Е</a>
                <a href="#">Ж</a>
                <a href="#">З</a>
                <a href="#">И</a>
                <a href="#">К</a>
                <a href="#">Л</a>
                <a href="#">М</a>
                <a href="#">Н</a>
                <a href="#">О</a>
                <a href="#">П</a>
                <a href="#">Р</a>
                <a href="#">С</a>
                <a href="#">Т</a>
                <a href="#">У</a>
                <a href="#">Ф</a>
                <a href="#">Х</a>
                <a href="#">Ц</a>
                <a href="#">Ч</a>
                <a href="#">Ш</a>
                <a href="#">Щ</a>
                <a href="#">Ы</a>
                <a href="#">Э</a>
                <a href="#">Ю</a>
                <a href="#">Я</a>
                <a href="#">Ё</a>
                <a href="#">Й</a>
                <a href="#">Ь</a>
                <a href="#">Ъ</a>
            </div>
        </div>
    </li>
</ul>