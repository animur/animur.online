﻿<!DOCTYPE html>
<html lang="ru">
<head>
    {headers}
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="../img/favicon.png"/>
    <link href="{THEME}/style/styles.css" type="text/css" rel="stylesheet"/>
    <link href="{THEME}/style/engine.css" type="text/css" rel="stylesheet"/>
    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>
</head>
<body>

<div class="wrap">
    <header class="head-wr">
        <div class="head clearfix">
            <div class="head-one  center" id="head-one">
                <a href="/" class="logotype" title="На главную"></a>
                <ul class="head-menu clearfix" id="head-menu">
                    <li><a href="#">Главная</a></li>
                    <li><a href="/anime/ongoing/">Онгоинги</a></li>
                    <li><a href="/anime/tv/">Завершенные</a></li>
                    <li><a href="#">В процессе</a></li>
                </ul>
                <div class="action-btns">
                    <div class="search-btn" id="searchbtn"><span class="fa fa-search"></span></div>
                    <div class="log-btn icon-left" id="loginbtn">
                        [group=5]<span class="fa fa-sign-in"></span><span>Авторизация</span>[/group]
                        [not-group=5]<span class="fa fa-bars"></span><span>Управление</span>[/not-group]
                    </div>
                </div>
            </div>
            <div>
                {include file="slyder.tpl"}
            </div>
            <nav class="head-two  center">
                {include file="main-menu.tpl"}
            </nav>
        </div>
        <div class="search-wrap" id="search-wrap">
            <form id="quicksearch" method="post">
                <input type="hidden" name="do" value="search"/>
                <input type="hidden" name="subaction" value="search"/>
                <div class="search-box">
                    <input id="story" name="story" placeholder="Поиск..." type="text"/>
                    <button type="submit" title="Найти">Найти</button>
                </div>
            </form>
        </div>
    </header>

    [aviable=main]
    <div class="carou-wr">
        <div class="carou center">
            <div id="owl-carou">
                {custom category="3" template="custom-carou" aviable="global" from="0" limit="20" cache="no"}
            </div>
        </div>
    </div>
    [/aviable]

    <div class="cols-r center clearfix" id="cols-r">
        <main class="content">
            [aviable=main|cat]
            <div class="section-title clearfix">
                <div class="grid-select clearfix" id="grid-select">
                    <div data-type="grid-list" class="current"><span class="fa fa-reorder"></span></div>
                    <div data-type="grid-thumb"><span class="fa fa-th"></span></div>
                </div>
                <h1 class="title">Последние ДОБАВЛЕНИЯ</h1>
                [sort]
                <div class="sorter" data-label="Сортировать по">
                    <span class="fa fa-chevron-down"></span>
                    {sort}
                </div>
                [/sort]
            </div>
            <div class="section-items clearfix grid-list" id="grid">
                {content}
            </div>
            [/aviable]
            [not-aviable=main|cat]
            <div class="speedbar">{speedbar}</div>
            <div class="full-wrap">
                {content}
            </div>
            [/not-aviable]
        </main>

        <aside class="sidebar clearfix">
            [aviable=main|cat]{include file="main-filter.tpl"}[/aviable]
            <div class="sidebox carouside">
                <div class="sidebox-t subtitle">ПРЕМЬЕРЫ</div>
                <div class="sidebox-c">
                    <div id="owl-carouside">
                        {custom category="3" template="custom-carouside" aviable="global" from="0" limit="5" cache="no"}
                    </div>
                </div>
            </div>
            <div class="sidebox">
                <div class="sidebox-t subtitle">Случайный онгоинг</div>
                <div class="sidebox-c">
                    {custom category="10" template="custom-side" aviable="global" from="0" limit="5" cache="no"}
                </div>
            </div>
            <div class="sidebox topsbox">
                <div class="sidebox-t subtitle">Топ за неделю</div>
                <ol class="sidebox-c tops">
                    {custom days="7" template="custom-top" order="rating" from="0" limit="5" cache="no"}
                </ol>
            </div>
            <div class="sidebox lcomms">
                <div class="sidebox-t subtitle">Последние комментарии</div>
                <div class="sidebox-c">
                    {customcomments template="custom-lcomm" aviable="global" from="0" limit="5" cache="yes"}
                </div>
                <a href="#" class="more-link"><span>Все комментарии</span></a>
            </div>
        </aside>

    </div>
    <!-- end cols-r -->

    [aviable=main]
    <div class="section center caroubottom">
        <div class="title">Мы рекомендуем</div>
        <div id="owl-caroubottom">
            {custom category="3" template="custom-carouside" aviable="global" from="0" limit="10" cache="no"}
        </div>
    </div>
    [/aviable]

    <footer class="foot-wr">
        <div class="foot center clearfix">
            <ul class="foot-menu clearfix">
                <li><a href="#">Главная</a></li>
                <li><a href="#">Ссылка</a></li>
                <li><a href="#">Ссылка</a></li>
                <li><a href="#">Ссылка</a></li>
                <li><a href="#">Ссылка</a></li>
            </ul>
            <div class="foot-text">
                Веками магия и неприступные стены защищали людей от любых напастей. Но древнее зло, побежденное и
                забытое тысячелетия назад, пробудилось.
            </div>
            <div class="count">код счетчика</div>
        </div>
    </footer>

</div>
<!-- end wrap -->
{login}
[not-aviable=search]{jsfiles}[/not-aviable]
<script src="{THEME}/js/libs.js"></script>
<link href="{THEME}/style/filter-xf.css" type="text/css" rel="stylesheet"/>
<link href="{THEME}/style/lis-head.css" type="text/css" rel="stylesheet"/>
<script src="{THEME}/js/filter-xf.js"></script>
<script src="{THEME}/js/lis-head.js"></script>
{AJAX}
</body>
</html>