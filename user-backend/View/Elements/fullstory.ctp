﻿<div class="full-wrap">
    <article class="movie-item movie-full">

        <div class="movie-cols clearfix">

            <div class="movie-img ignore-select">
                <div class="movie-poster">
                    [xfvalue_image]
                </div>

                <div class="mb-online icon-left scrolla"><span class="fa fa-play"></span>Смотреть онлайн</div>
                [xfgiven_dnei]
                <div id="timer"
                     style="bottom:0px;font-family:'BebasBold', sans-serif; color:#fff;text-align:center;margin-right:10px; font-size:15px;background:rgba(0,0,0,.5); width:100%;padding-top:5px;padding-bottom:5px;">
                    <div class="do_ocnalos" style="font-size: 15px;color: #f4f6fa;font-weight: 700;font-size: 16px;">До
                        выхода следующей серии осталось:
                    </div>
                    <div id="countdown" style="font-size: 15px;color: #fff;font-weight: 700;font-size: 16px;"></div>
                    <script type="text/javascript" src="{THEME}/js/timer2.js">
                    </script>
                    <script> $(function () {

                            var note = $('#note'),
                                // это дата конца таймера
                                ts = new Date([xfvalue_god], [xfvalue_mesyaca - goda], [xfvalue_dnei])
                            // это дата конца таймера
                            newYear = true;


                            $('#countdown').countdown({
                                timestamp: ts,
                                callback: function (days, hours, minutes, seconds) {

                                    var message = "";

                                    message += "Дней: " + days + ", ";
                                    message += "часов: " + hours + ", ";
                                    message += "минут: " + minutes + " и ";
                                    message += "секунд: " + seconds + " <br />";

                                    if (newYear) {
                                        message += "";
                                    }
                                    else {
                                        message += "";
                                    }

                                    note.html(message);
                                }
                            });

                        });
                    </script>
                </div>
                [/xfgiven_dnei]


                <div class="screens">
                    <center>
                        <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"
                                charset="utf-8"></script>
                        <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
                        <div class="ya-share2"
                             data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter"></div>
                    </center>
                </div>

            </div>

            <div class="movie-text">

                <div class="movie-title">
                    <h1>[xfgiven_rus_title][xfvalue_rus_title][/xfgiven_rus_title] </h1>
                    <div class="movie-original">
                        [xfgiven_original_title][xfvalue_original_title][/xfgiven_original_title]
                    </div>
                    <div class="movie-meta">
                        [xfgiven_quality]
                        <div class="meta-qual">[xfvalue_quality]</div>
                        [/xfgiven_quality]
                        <div class="meta-date">{date}</div>
                    </div>
                </div>

                <div class="full-text">{full-story}</div>

                <ul class="movie-lines">
                    [xfgiven_year]
                    <li>
                        <div class="ml-label">Год выпуска:</div>
                        <div class="ml-desc">[xfvalue_year]</div>
                    </li>
                    [/xfgiven_year]
                    [xfgiven_time]
                    <li>
                        <div class="ml-label">Длительность:</div>
                        <div class="ml-desc">[xfvalue_time]</div>
                    </li>
                    [/xfgiven_time]
                    [xfgiven_strana]
                    <li>
                        <div class="ml-label">Страна:</div>
                        <div class="ml-desc">[xfvalue_strana]</div>
                    </li>
                    [/xfgiven_strana]
                    [xfgiven_janre]
                    <li>
                        <div class="ml-label">Жанр:</div>
                        <div class="ml-desc">[xfvalue_janre]</div>
                    </li>
                    [/xfgiven_janre]
                    [xfgiven_director]
                    <li>
                        <div class="ml-label">Режиссер:</div>
                        <div class="ml-desc">[xfvalue_director]</div>
                    </li>
                    [/xfgiven_director]
                    [xfgiven_rolax]
                    <li>
                        <div class="ml-label">Озвучивание::</div>
                        <div class="ml-desc">[xfvalue_rolax]</div>
                    </li>
                    [/xfgiven_rolax]
                    [xfgiven_tayming]
                    <li>
                        <div class="ml-label">Тайминг:</div>
                        <div class="ml-desc">[xfvalue_tayming]</div>
                    </li>
                    [/xfgiven_tayming]
                    [xfgiven_age]
                    <li>
                        <div class="ml-label">Возраст:</div>
                        <div class="ml-desc">[xfvalue_age]</div>
                    </li>
                    [/xfgiven_age]
                </ul>

                <div class="movie-bottom clearfix ignore-select">
                    <div class="mb-meta icon-left"><span class="fa fa-commenting"></span>{comments-num}</div>
                    <div class="mb-meta icon-left"><span class="fa fa-eye"></span>{views}</div>
                    <div class="movie-tools">
				<span>
                   
                    </span>
                        <span>
					{favorites}
					
				</span>
                    </div>
                </div>

                <div class="rates ignore-select">
                    <div class="db-rates">

                        ЧТО,

                    </div>
                    [rating]{rating}[/rating]
                </div>

            </div>

        </div>


        <div class="tabs-box ignore-select">
            <div class="tabs-sel">
                <span class="current">Смотреть онлайн</span>
            </div>

            <div class="tabs-b video-box visible">

                {include file="Player.tpl"}
                {include file=" plaier2.tpl"}

            </div>
            <div class="mov-compl icon-left">[complaint]<span class="fa fa-bug"></span>Сообщить об ошибке[/complaint]
            </div>
        </div>

        <div class="ignore-select">

            <br>
        </div>

        <div class="rels caroubottom">
            <div class="rel-t">Рекомендуем также к просмотру:</div>
            <div class="rel-c" id="owl-rels">
                {custom category="1,2,3,4,5,6,7" template="relatednews" aviable="global" order="rand" limit="15"
                cache="no"}

            </div>
        </div>

    </article>
    [group=5]
    <div class="berrors">
        <div class="info"> Уважаемый посетитель, Вы зашли на сайт как незарегистрированный пользователь. Мы рекомендуем
            Вам зарегистрироваться, либо зайти на сайт под своим логином.
        </div>
    </div>
    [/group]

    <div class="full-comms ignore-select" id="full-comms">
        <div class="comms-head clearfix">
            <div class="comms-title">Комментарии <sup>{comments-num}</sup></div>
            <div class="add-commbtn button icon-left" id="add-commbtn"><span class="fa fa-plus"></span>Комментировать
            </div>
        </div>
        <!--noindex-->

        {addcomments}

        <!--/noindex-->
    </div>

    {comments}

</div>