<li>
    <a class="top-item" href="{full-link}">
        [newscount=1]
        <div class="top-img img-box">
            <img src="[xfvalue_image]" alt="{title}"/>
        </div>
        [/newscount]
        <div class="carouside-title">
            <p class="nowrap">{title limit="40"}</p>
            <span>[xfgiven_original_title][xfvalue_original_title][/xfgiven_original_title]</span>
        </div>
    </a>
</li>