﻿<article class="movie-item">
    <div class="movie-cols clearfix">
        <div class="movie-img img-box pseudo-link" data-link="{full-link}">
            <img src="[xfvalue_image]" alt="{title}"/>
            <div class="poster-label">[xfvalue_year]</div>
        </div>
        <div class="movie-text">
            <div class="movie-title">
                <a href="{full-link}">{title limit="80"}</a>
                <div class="movie-original">[xfgiven_original_title][xfvalue_original_title][/xfgiven_original_title]
                </div>
                <div class="movie-meta">
                    <div class="meta-qual" style="text-align: center;">[xfvalue_quality]</div>
                    <div class="meta-date">{date=j F Y}</div>
                    [category=10]
                    <div class="status btn-tooltip status-2" title="Сериал закрыт, продолжения больше не будет"
                         style="float: right;position: relative;display: inline-block;color: #ff293d;font-size: 12px;font-weight: 500;line-height: 20px;margin-right: 20px;">
                        <span class="ico">Завершенные</span>
                    </div>
                    [/category]
                </div>
            </div>
            <ul class="movie-lines">
                [xfgiven_12]
                <li>
                    <div class="ml-label">[xfvalue_12]:</div>
                    <div class="ml-desc">{author}</div>
                </li>
                [/xfgiven_12]
                [xfgiven_istochnik]
                <li>
                    <div class="ml-label">Источник:</div>
                    <div class="ml-desc">[xfvalue_istochnik]</div>
                </li>
                [/xfgiven_istochnik]
                [xfgiven_year]
                <li>
                    <div class="ml-label">Год выпуска:</div>
                    <div class="ml-desc">[xfvalue_year]</div>
                </li>
                [/xfgiven_year]
                [xfgiven_series]
                <li>
                    <div class="ml-label">Добавлена:</div>
                    <div class="ml-desc">[xfvalue_series] серия</div>
                </li>
                [/xfgiven_series]
                [xfgiven_time]
                <li>
                    <div class="ml-label">Длительность:</div>
                    <div class="ml-desc">[xfvalue_time]</div>
                </li>
                [/xfgiven_time]
                [xfgiven_strana]
                <li>
                    <div class="ml-label">Страна:</div>
                    <div class="ml-desc">[xfvalue_strana]</div>
                </li>
                [/xfgiven_strana]
                [xfgiven_janre]
                <li>
                    <div class="ml-label">Жанр:</div>
                    <div class="ml-desc">[xfvalue_janre]</div>
                </li>
                [/xfgiven_janre]
                [xfgiven_director]
                <li>
                    <div class="ml-label">Режиссер:</div>
                    <div class="ml-desc">[xfvalue_director]</div>
                </li>
                [/xfgiven_director]
                [xfgiven_rolax]
                <li>
                    <div class="ml-label">Озвучивание:</div>
                    <div class="ml-desc">[xfvalue_rolax]</div>
                </li>
                [/xfgiven_rolax]
            </ul>
            <div class="movie-desc">{short-story}</div>

        </div>
    </div>
    <div class="movie-bottom clearfix">
        <div class="mb-online icon-left"><span class="fa fa-play"></span>Смотреть онлайн</div>
        <div class="mb-meta icon-left"><span class="fa fa-commenting"></span>{comments-num}</div>
        <div class="mb-meta icon-left"><span class="fa fa-eye"></span>{views}</div>
        [not-group=5]
        <div class="movie-tools">
            <span>[edit]<span class="fa fa-bars" title="Редактировать"></span>[/edit]</span>
            <span>
			[add-favorites]<span class="fa fa-heart-o" title="Добавить в закладки"></span>[/add-favorites]
			[del-favorites]<span class="fa fa-heart" title="Убрать из закладок"></span>[/del-favorites]
		</span>
        </div>
        [/not-group]
    </div>
</article>