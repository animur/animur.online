<a class="carouside-item" href="{full-link}">
    <div class="carouside-img img-box">
        <img src="[xfvalue_image]" alt="{title}"/>
        <div class="poster-label">[xfvalue_year]</div>
    </div>
    <div class="carouside-title">
        <p class="nowrap">{title limit="40"}</p>
        <span>[xfgiven_original_title][xfvalue_original_title][/xfgiven_original_title]</span>
    </div>
</a>