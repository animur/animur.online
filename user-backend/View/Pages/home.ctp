<div class="wrap">
    <header class="head-wr">
        <div class="head clearfix">
            <div class="head-one  center" id="head-one">
                <a href="/" class="logotype" title="На главную"></a>
                <ul class="head-menu clearfix" id="head-menu">
                    <li><a href="#">Главная</a></li>
                    <li><a href="/anime/ongoing/">Онгоинги</a></li>
                    <li><a href="/anime/tv/">Завершенные</a></li>
                    <li><a href="#">В процессе</a></li>
                </ul>
                <?= $this->Element('../Users/login', [$authUser]) ?>
                <div class="action-btns">
                    <div class="search-btn" id="searchbtn"><span class="fa fa-search"></span></div>
                    <div class="log-btn icon-left" id="loginbtn">
                        <?php
                        if ($authUser) {
                            echo '<span class="fa fa-bars"></span><span>Управление</span>';
                        } else {
                            echo '<span class="fa fa-sign-in"></span><span>Авторизация</span>';
                        } ?>
                    </div>
                    <?php
                    if (!$authUser) {
                        echo $this->Html->link('Регистрация', '#',
                            ['class' => 'log-register log-btn', 'onclick' => 'ReadFile("users/register")']);
                    }
                    ?>
                </div>
            </div>
            <div>
                <?= $this->Element('../Elements/slider') ?>
            </div>
            <nav class="head-two center">
                <?= $this->Element('../Elements/main-menu') ?>
            </nav>
        </div>
        <div class="search-wrap" id="search-wrap">
            <form id="quicksearch" method="post">
                <input type="hidden" name="do" value="search"/>
                <input type="hidden" name="subaction" value="search"/>
                <div class="search-box">
                    <input id="story" name="story" placeholder="Поиск..." type="text"/>
                    <button type="submit" title="Найти">Найти</button>
                </div>
            </form>
        </div>
    </header>

    <div class="carou-wr">
        <div class="carou center">
            <div id="owl-carou">
                <?= $this->Element('../Elements/custom-carou') ?>
            </div>
        </div>
    </div>
    <div class="cols-r center clearfix" id="cols-r">
        <main class="content">
            <div class="section-title clearfix">
                <div class="grid-select clearfix" id="grid-select">
                    <div data-type="grid-list" class="current"><span class="fa fa-reorder"></span></div>
                    <div data-type="grid-thumb"><span class="fa fa-th"></span></div>
                </div>
                <h1 class="title">Последние ДОБАВЛЕНИЯ</h1>
                <div class="sorter" data-label="Сортировать по">
                    <span class="fa fa-chevron-down"></span>
                </div>
            </div>
            <div class="section-items clearfix grid-list" id="grid">
            </div>
            <div class="speedbar"></div>
            <div class="full-wrap">
            </div>
        </main>

        <aside class="sidebar clearfix">
            <?= $this->Element('../Elements/main_filter') ?>
            <div class="sidebox carouside">
                <div class="sidebox-t subtitle">ПРЕМЬЕРЫ</div>
                <div class="sidebox-c">
                    <div id="owl-carouside">
                        <?= $this->Element('../Elements/custom-carouside') ?>
                    </div>
                </div>
            </div>
            <div class="sidebox">
                <div class="sidebox-t subtitle">Случайный онгоинг</div>
                <div class="sidebox-c">
                    <?= $this->Element('../Elements/custom-side') ?>
                </div>
            </div>
            <div class="sidebox topsbox">
                <div class="sidebox-t subtitle">Топ за неделю</div>
                <ol class="sidebox-c tops">
                    <?= $this->Element('../Elements/custom-top') ?>
                </ol>
            </div>
            <div class="sidebox lcomms">
                <div class="sidebox-t subtitle">Последние комментарии</div>
                <div class="sidebox-c">
                    <?= $this->Element('../Elements/custom-lcomm') ?>
                </div>
                <a href="#" class="more-link"><span>Все комментарии</span></a>
            </div>
        </aside>

    </div>
    <!-- end cols-r -->

    <div class="section center caroubottom">
        <div class="title">Мы рекомендуем</div>
        <div id="owl-caroubottom">
            <?= $this->Element('../Elements/custom-carouside') ?>
        </div>
    </div>

    <footer class="foot-wr">
        <div class="foot center clearfix">
            <ul class="foot-menu clearfix">
                <li><a href="#">Главная</a></li>
                <li><a href="#">Ссылка</a></li>
                <li><a href="#">Ссылка</a></li>
                <li><a href="#">Ссылка</a></li>
                <li><a href="#">Ссылка</a></li>
            </ul>
            <div class="foot-text">
                Веками магия и неприступные стены защищали людей от любых напастей. Но древнее зло, побежденное и
                забытое тысячелетия назад, пробудилось.
            </div>
            <div class="count">код счетчика</div>
        </div>
    </footer>

</div>

<script>
    function ReadFile(url) {
        $.ajax({
            url: url,
            cache: false,
            type: 'GET',
            dataType: 'HTML',
            success: function (data) {
                var data = $(data).filter('#container');
                console.log(data);
                $('.carou-wr').hide();
                $('.content').html(data);
            }
        });
    }
</script>